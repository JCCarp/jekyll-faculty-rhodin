---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am an Assistant Professor at UBC. Prior to that, I was lecturer at EPFL and postdoctoral researcher in the Computer Vision Lab of Pascal Fua and PhD candidate in the GVV group of Christian Theobalt at the Max-Plack-Institute for Informatics.
Check out [Linkedin/Rhodin] for a timeline.


## Research Interests

My research interests range from 3D computer vision, over machine learning, to computer graphics and augmented reality. With the aim of enabling tight and rich, yet natural and non-intrusive computer-human interaction, I advance 3D vision algorithms to yield high-quality dynamic reconstructions of our everyday environment from plain video input. The latest VR and AR devices offer incredible display capabilities; my 3D vision projects complement them to aid mutual human-computer interaction in our everyday life.

## Selected Publications

{% bibliography --file selected.bib %}
